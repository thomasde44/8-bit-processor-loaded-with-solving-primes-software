LIBRARY ieee; USE ieee.std_logic_1164.ALL; USE ieee.numeric_std.ALL;
ENTITY Proc9090sim2020 IS END Proc9090sim2020;
ARCHITECTURE aaa OF Proc9090sim2020 IS 
type myArray is array (0 to 32) of unsigned(7 downto 0); 
signal Mem:myArray:=("00000110", "10000000", "00001110", "00000010", "11110010", "00000111", "11000000", "00011110", "00000000", "00010110", "00000010", "11001010", "00001110",
"00011000", "01101001", "10100110", "00000000", "11111010", "00010100", "00010101", "00011100", "00010100", "11000011", "00001011", "11011111", "00011011", "00011100", "01110001", "00110110","00000000",
"00001100", "11000011", "00000100");
type myRegs is array (0 to 7) of unsigned(7 downto 0); 
signal Reg:myRegs:=(x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00");
signal clk,RSTc : bit := '1';
signal PC,IR,Tc: unsigned(7 downto 0) := x"00"; 
signal Cy, Zf, rZr : unsigned(0 downto 0) := "0"; -- rZr is division remainder = 0
begin 
clk <= not clk after 10 ns;
RSTc <= '0' after 25 ns;

process(clk,RSTc)
variable src,dst, dsrc,ddst : integer;
variable tmp17: unsigned(16 downto 0);
variable quo: unsigned(7 downto 0):="00000000";

begin
if (RSTc='1') then Tc <= x"00";
elsif (clk'event and clk='1') then 
   Tc <= Tc+1;
   CASE Tc is
     when x"00" => IR <= Mem(to_integer(PC));
                    PC <= PC+1;
     when x"01" => 
        src := to_integer(IR(2 downto 0));
        dst := to_integer(IR(5 downto 3));
        CASE IR(7 downto 6) is
          when "00" =>   --- so far, just setting Zf on INR/DCR -- later, set more flags appropriately
             if IR(2 downto 0)="100" then Reg(dst)<=Reg(dst)+1; Zf<="0"; if Reg(dst)+1=x"00" then Zf<="1"; end if;Tc<=x"00"; end if;
             if IR(2 downto 0)="101" then Reg(dst)<=Reg(dst)-1; Zf<="0"; if Reg(dst)-1=x"00" then Zf<="1"; end if;Tc<=x"00"; end if;
             if IR(5 downto 0)="010111" then  -- rotate left
                           Reg(7) <= Reg(7)(6 downto 0) & Cy;
                           Cy<=Reg(7)(7 downto 7);Tc<=x"00"; -- set after clock
                           end if;
             if IR(5 downto 0)="011111" then -- rotate right
                           Reg(7) <= Cy & Reg(7)(7 downto 1); 
                           Cy<=Reg(7)(0 downto 0); Tc<=x"00";
                           end if;
             if IR(3 downto 0)="1001" then                             -- DAD
                           dsrc := to_integer(IR(5 downto 4))*2;
                           tmp17 := ("0" & Reg(4) & Reg(5)) + ("0" & Reg(dsrc) & Reg(dsrc+1));
                           Reg(5) <= tmp17(7 downto 0);
                           Reg(4) <= tmp17(15 downto 8);
                           Cy <= tmp17(16 downto 16);Tc<=x"00";
                           end if;
             if IR(2 downto 0)="110" then Reg(dst) <= Mem(to_integer(PC));  -- load Rdst, mem[i]
                                                PC <= PC+1; Tc<=x"00";  end if;
             if IR(3 downto 0)="0001" then -- LXI Reg(dst), PC+1
                           ddst := to_integer(IR(5 downto 4))*2;
                           Reg(ddst+1)<=Mem(to_integer(PC));
                           PC <= PC+1; end if;
             if IR(5 downto 0) = "000011" then -- 8bit * 8bit, result stored in reg 7 and 6;
                           tmp17 := "000000000"&Reg(6);
                           PC <= PC + 1;  end if;
                           
          when "01" => Reg(dst) <= Reg(src);
             Tc<=x"00"; 
          when "10" =>
             if(IR(5 downto 3)="000") then Reg(7)<=Reg(7)+Reg(src);Tc<=x"00"; end if;
             if IR(3 downto 0)="0110" then -- divide begins, stored in memory like xxxxxxxx(IR), xxxxxxxx(first 8 bits dividend)
                ddst := to_integer(IR(5 downto 4))*2;
                tmp17 := ("0" & Mem(to_integer(PC)) & Reg(5));  
                Reg(ddst) <= Reg(2); PC <= PC + 2; end if;
          when "11" => 
             if IR(5 downto 0)="101011" then Reg(4)<=Reg(2); Reg(2)<=Reg(4); Reg(5)<=Reg(3); Reg(3)<=Reg(5); end if;
             if IR(5 downto 0)="000011" then  PC<=Mem(to_integer(PC));  end if;
             if IR(5 downto 0)="010010" then if Cy="0" then PC<=Mem(to_integer(PC)); else PC<=PC+1; end if; end if;
             if IR(5 downto 0)="000010" then if Zf="0" then PC<=Mem(to_integer(PC)); else PC<=PC+1; end if; end if;
             if IR(5 downto 0)="111010" then if rZr="1" then PC<=Mem(to_integer(PC)); else PC <= Mem(to_integer(PC+1));  end if; end if;
             if IR(2 downto 0)="111" then if Reg(dst) = "00000000" then PC <= Mem(to_integer(PC)); else PC <= Mem(to_integer(PC + 1)); end if; end if; -- CMP Reg[dst] with one
             if IR(5 downto 0)="110010" then if Reg(1) < Reg(0) then PC <= Mem(to_integer(PC)); else PC <= Mem(to_integer(PC + 1)); end if; end if; -- cmp i < 128 (Reg(1) < Reg(0))
             if IR(5 downto 0)="001010" then if Reg(2) < Reg(1) then PC <= Mem(to_integer(PC)); else PC <= Mem(to_integer(PC + 1)); end if; end if; -- cmp j < i (Reg(2) < Reg(1))
            -- if IR(5)='0' then PC<=PC+("000" & IR(4 downto 0)); end if;  -- original "branch" instruction
            -- if IR(5)='1' then PC<=PC+("111" & IR(4 downto 0)); end if;  -- original "branch" instruction
             Tc<=x"00";
          when others => 
          end case;
     when x"02" => ---  LXI will flow here -- possibly other commands later
        CASE IR(7 downto 6) is
          when "00" =>
             if IR(3 downto 0)="0001" then  -- LXI, Reg(dst), second byte (PC)
                       --  ddst := to_integer(IR(5 downto 4))*2; -- already done?
                           Reg(ddst)<=Mem(to_integer(PC)); -- second byte (little Endian)
                           PC <= PC+1;end if;
             if IR(3 downto 0)="0011" then -- multiply first shift or add
                           if tmp17(0 downto 0) = 0 then
                            tmp17 := '0' & tmp17(16 downto 1);
                            PC <= PC + 1;
                           else
                            tmp17 := tmp17 + ('0' & Reg(7)& "00000000") ;
                            tmp17 := '0' & tmp17(16 downto 1);
                            PC <= PC + 1;
                           end if;
             end if;
          when "10" =>
             if IR(3 downto 0) = "0110" then -- divide one

                if tmp17(16 downto 7) >= ("00"&Reg(dst)) then
                    tmp17(16 downto 7) := tmp17(16 downto 7) - ("00"&Reg(dst));

                    quo(7 downto 7) := not  quo(7 downto 7);
                    PC <= PC + 1;
                    else 
                    PC <= PC + 1; 
                    end if;
             end if;
     --        Reg(0) <= tmp17(15 downto 8);
     --        Reg(1) <= tmp17(7 downto 0);          
     --        Reg(3) <= quo; 
          when others =>
        end case;
     when x"03" =>
        CASE IR(7 downto 6) is
          when "00" =>
            if IR(3 downto 0) = "0011" then -- multiply second shift or add
                           if tmp17(0 downto 0) = 0 then
                            tmp17 := '0' & tmp17(16 downto 1);
                            PC <= PC + 1;
                           else
                            tmp17 := tmp17 + ('0' & Reg(7)& "00000000") ;
                            tmp17 := '0' & tmp17(16 downto 1);
                            PC <= PC + 1;
                           end if;
           end if;
          when "10" => -- divide two
             if IR(3 downto 0) = "0110" then

                if tmp17(16 downto 6) >= ("000"&Reg(ddst)) then
                    tmp17(16 downto 6) := (tmp17(16 downto 6) - ("000"&Reg(ddst)));
                    quo(6 downto 6) := not quo(6 downto 6);
                    PC <= PC + 1; 
                    else 
                    PC <= PC + 1;
                    end if; 
             end if;
      --       Reg(0) <= tmp17(15 downto 8);
      --       Reg(1) <= tmp17(7 downto 0);          
      --       Reg(3) <= quo; 
        when others =>
        end case;
     when x"04" =>
        CASE IR(7 downto 6) is
       when "00" =>
         if IR(3 downto 0) = "0011" then -- multiply third shift or add
                        if tmp17(0 downto 0) = 0 then
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        else
                         tmp17 := tmp17 + ('0' & Reg(7)& "00000000" );
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        end if;
        end if;
      when "10" =>
        if IR(3 downto 0) = "0110" then -- divide 3

            if  tmp17(16 downto 5) >= ("0000" & Reg(ddst)) then
                tmp17(16 downto 5) := ( tmp17(16 downto 5) - ("0000" & Reg(ddst)) );
                quo(5 downto 5) := not quo(5 downto 5);
                PC <= PC + 1; 
                else 
                    PC <= PC + 1;
                end if;
        end if;
    --    Reg(0) <= tmp17(15 downto 8);
    --    Reg(1) <= tmp17(7 downto 0);          
    --    Reg(3) <= quo; 
     when others =>
        end case;
     when x"05" =>
        CASE IR(7 downto 6) is
       when "00" =>
         if IR(3 downto 0) = "0011" then -- multiply fourth shift or add
                        if tmp17(0 downto 0) = 0 then
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        else
                         tmp17 := tmp17 + ('0' & Reg(7)& "00000000") ;
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        end if;
        end if;
      when "10" => -- divide four

        if IR(3 downto 0) = "0110" then
            if  tmp17(16 downto 4) >= ("00000" & Reg(ddst)) then
                tmp17(16 downto 4) := ( tmp17(16 downto 4) - ("00000" & Reg(ddst)) );
                quo(4 downto 4) := not quo(4 downto 4);
                PC <= PC + 1; 
                else 
                    PC <= PC + 1;
                end if;
        end if;
    --    Reg(0) <= tmp17(15 downto 8);
    --    Reg(1) <= tmp17(7 downto 0);          
   --     Reg(3) <= quo; 
     when others =>
        end case;
     when x"06" =>
        CASE IR(7 downto 6) is
       when "00" =>
         if IR(3 downto 0) = "0011" then -- multiply fifth shift or add
                        if tmp17(0 downto 0) = 0 then
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        else
                         tmp17 := tmp17 + ('0' & Reg(7)& "00000000") ;
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        end if;
        end if;
      when "10" => -- divide five
       
        if IR(3 downto 0) = "0110" then
            if  tmp17(16 downto 3) >= ("000000" & Reg(ddst))  then
                tmp17(16 downto 3) := ( tmp17(16 downto 3) - ("000000" & Reg(ddst)) );
                quo(3 downto 3) := not quo(3 downto 3);
                PC <= PC + 1; 
                else 
                    PC <= PC + 1;
                end if;
        end if;
    --    Reg(0) <= tmp17(15 downto 8);
    --    Reg(1) <= tmp17(7 downto 0);     
    --    Reg(3) <= quo;   
     when others =>
        end case;
     when x"07" =>
        CASE IR(7 downto 6) is
       when "00" =>
         if IR(3 downto 0) = "0011" then -- multiply sixth shift or add
                        if tmp17(0 downto 0) = 0 then
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        else
                         tmp17 := tmp17 + ('0' & Reg(7)& "00000000") ;
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        end if;
        end if;
      when "10" => -- divide six

          if IR(3 downto 0) = "0110" then
              if tmp17(16 downto 2) >= ("0000000" & Reg(ddst)) then
                  tmp17(16 downto 2) := ( tmp17(16 downto 2) - ("0000000" & Reg(ddst)) );
                  quo(2 downto 2) := not quo(2 downto 2);
                  PC <= PC + 1; 
                  else
                    PC <= PC + 1;
                  end if;
          end if;
     --     Reg(0) <= tmp17(15 downto 8);
   --       Reg(1) <= tmp17(7 downto 0);
  --        Reg(3) <= quo; 
     when others =>
        end case;
     when x"08" =>
        CASE IR(7 downto 6) is
       when "00" =>
         if IR(3 downto 0) = "0011" then -- multiply seventh shift or add
                        if tmp17(0 downto 0) = 0 then
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        else
                         tmp17 := tmp17 + ('0' & Reg(7)& "00000000") ;
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        end if;
        end if;
      when "10" => -- divide seven
    
            if IR(3 downto 0) = "0110" then
                if  tmp17(16 downto 1) >= ("00000000" & Reg(ddst))  then
                    tmp17(16 downto 1) := ( tmp17(16 downto 1) - ("00000000" & Reg(ddst)) );
                    quo(1 downto 1) := not quo(1 downto 1);
                    PC <= PC + 1; 
                    else
                        PC <= PC + 1;
                    end if;
            end if;
       --     Reg(0) <= tmp17(15 downto 8);
     --       Reg(1) <= tmp17(7 downto 0);    
       --     Reg(3) <= quo; 
     when others =>
        end case;
     when x"09" =>
        CASE IR(7 downto 6) is
       when "00" =>

         if IR(3 downto 0) = "0011" then -- multiply eigth (last) shift or add
                        if tmp17(0 downto 0) = 0 then
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        else
                         tmp17 := tmp17 + ('0' & Reg(7)& "00000000") ;
                         tmp17 := '0' & tmp17(16 downto 1);
                         PC <= PC + 1;
                        end if;
        end if;
      when "10" => -- divide eight
   
            if IR(3 downto 0) = "0110" then
                if  tmp17(16 downto 0) >= ("000000000" & Reg(ddst)) then
                    tmp17(16 downto 0) := ( tmp17(16 downto 0) - ("000000000" & Reg(ddst)) );
                    quo(0 downto 0) := not quo(0 downto 0);
                    PC <= PC + 1; 
                    else 
                        PC <= PC + 1;
                    end if;
            end if;
           -- Reg(0) <= tmp17(15 downto 8);
           -- Reg(1) <= tmp17(7 downto 0);
           -- Reg(3) <= quo;      
     when others =>
        end case;
     when x"0A" =>
        CASE IR(7 downto 6) is
         when "00" =>
            if IR(3 downto 0) = "0011" then -- store result in registers
                         Reg(6) <= tmp17(15 downto 8);
                         Reg(7) <= tmp17(7 downto 0);
                         Tc <= x"00";
                         PC <= PC - 8;
            else
             Tc <= x"00";
            end if;  
         when "10" => -- divide finished
            if IR(3 downto 0) = "0110" then
                Reg(7) <= quo;
                PC <= PC - 9;
                if tmp17 = "00000000000000000" then rZr <= "1"; else rZr <= "0"; end if; -- set the remainder zero flag after division is complete
                Tc <= x"00";
            end if;
         when others =>
         end case;
     when others => 
     end case;
end if; end process; END;
